echo 'Starting to clone Trees needed for your device'

echo 'Firmware tree [1/1]'
#Firmware Tree
git clone --depth 1 https://gitlab.pixelexperience.org/android/vendor-blobs/vendor_xiaomi-firmware.git -b thirteen vendor/xiaomi-firmware

echo 'Cloning Vendor tree [2/3]'
# Vendor Tree
git clone --depth 1 https://gitlab.pixelexperience.org/android/vendor-blobs/vendor_xiaomi_violet.git -b thirteen vendor/xiaomi/violet

echo 'Cloning Kernel tree [3/3]'
# Kernel Tree
git clone --depth 1 https://github.com/kibria5/android_kernel_xiaomi_violet.git -b thirteen kernel/xiaomi/violet

echo 'Completed, Now proceeding to lunch'
